import React, { Component } from "react";
import { connect } from "socket.io-client";

class App extends Component {

    socket;

    join() {
        this.socket.emit("client.join", {});
    }
    send() {
        this.socket.emit("client.send", {
            text: "monyet gan",
        });
    }
    leave() {
        this.socket.emit("client.leave", {});
    }

    componentDidMount = () => {
        this.socket = connect("192.168.10.246:4444", {
            transports: ["websocket"],
        });

        this.socket.on("connect", (sock) => {
            console.log("connected", sock);
            this.socket.emit("auth.register", {
                id: 1,
                data: {
                    version: 1,
                },
            });
        });

        this.socket.on("auth.register", (resp) => {
            console.log(resp);
        });

        this.socket.on("client.join", (resp) => {
            console.log(resp);
        });

        this.socket.on("client.send", (resp) => {
            console.log(resp);
        });

        this.socket.on("client.leave", (resp) => {
            console.log(resp);
        });

        this.socket.on("example", (resp) => {
            console.log(resp);
        });
    }

    render() {
        return (
            <div style={{ textAlign: "center" }}>
                <button onClick={() => {
                    this.send();
                    // this.socket.close();
                }}
                >send</button>
                <button onClick={() => {
                    this.join();
                    // this.socket.close();
                }}
                >join</button>
                <button onClick={() => {
                    this.leave();
                    // this.socket.close();
                }}
                >leave</button>
            </div>
        )
    }
}
export default App;
